(function ($) {
  $.extend($.fn, {
    first: function () { return this.eq(0); },
    last: function () { return this.eq(-1); }
  });
}(window.tire));
